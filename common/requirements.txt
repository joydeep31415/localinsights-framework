beautifulsoup4==4.5.3
DateTime==4.1.1
python-dateutil==2.6.0
pytz==2016.10
six==1.10.0
zope.interface==4.3.3
requests==2.13.0
lxml