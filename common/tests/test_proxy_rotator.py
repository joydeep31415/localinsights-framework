from scraper.proxy_rotator import ProxyRotator
from scraper.proxy_rotator import get_proxies_from_urls
from scraper.proxy_rotator import save_proxies_from_buyproxies
from scraper.exceptions import ScraperError

import os
import unittest
from unittest import mock


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, text_data, status_code):
            self.text_data = text_data
            self.status_code = status_code

        @property
        def text(self):
            return self.text_data

    if args[0] == 'http://someurl.com/somethingsomething':
        return MockResponse('abc\ndef', 200)
    else:
        return MockResponse('1\n2', 200)

    return MockResponse('', 404)


class TestProxyRotator(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_proxy(self, mock_get):
        pr = ProxyRotator()
        res = pr.proxies
        self.assertIn("http://locinsights",
                      res["http"],
                      "http gives a web url")
        self.assertIn("http://locinsights",
                      res["https"],
                      "https has a valid proxy")


class TestProxyApiCalling(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_get_proxies_from_urls(self, mock_get):
        res = get_proxies_from_urls('http://someurl.com/somethingsomething')
        self.assertEqual(res, 'abc\ndef')

    def test_save_proxies_from_buyproxies(self):
        peas = 'a\nb\nc'
        save_proxies_from_buyproxies(peas)
        with open("buyproxies.txt") as f:
            f, s, t = f.readlines()

        self.assertEqual(list([f.rstrip(), s.rstrip(), t.rstrip()]), ['a', 'b', 'c'])

        os.remove("buyproxies.txt")

    def test_get_proxies_from_urls_invalidurl(self):
        with self.assertRaises(ScraperError) as context:
            get_proxies_from_urls('http://somethinginvalid.org')

    def test_change_buyproxies_to_proxy_list(self):
        pr = ProxyRotator()

        # establish the buy proxies
        peas = 'a\nb\nc'
        save_proxies_from_buyproxies(peas)

        buyproxies = "buyproxies.txt"
        pr.change_buyproxies_to_proxy_list(buyproxies)

        with open(pr.path) as f:
            self.assertEqual(f.readlines(), ['a\n', 'b\n', 'c'])

        # the below should not throw error as buyproxies should get renamed to
        # the path file
        os.remove(pr.path)

    def test_change_buyproxies_to_proxy_list_fileisempty(self):
        pr = ProxyRotator()

        # establish the buy proxies
        peas = ''
        save_proxies_from_buyproxies(peas)

        buyproxies = "buyproxies.txt"
        pr.change_buyproxies_to_proxy_list(buyproxies)

        if os.path.isfile(pr.path):
            self.fail()
        else:
            self.assertTrue(1)

        # self.assertRaises(FileNotFoundError, open, pr.path)

        # the buyproxies file should still be there so the below line should not
        # throw any error
        os.remove(buyproxies)


if __name__ == "__main__":
    unittest.main()
