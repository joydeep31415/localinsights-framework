import scraper
from scraper.utils import setup_logging
from scraper.utils import file_is_empty
from scraper.utils import parse_date
from scraper.utils import setup_date_range
from scraper.utils import save_to_csv
from scraper.utils import create_csv
from scraper.utils import get_remaining_apn_list
from scraper.utils import add_identifiers
from scraper.utils import date_range
from scraper.utils import is_production_server

import unittest
import os
import datetime
from unittest.mock import patch


def get_number_of_lines(filename):
    with open(filename) as f:
        num_lines = sum(1 for line in f)
    return num_lines


def truncate_log_file(filename):
    with open(filename, 'w'):
        pass


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.logfile = "/tmp/tmpfile.log"
        self.htmlpage = "/tmp/page.html"

    def tearDown(self):
        self.delete_file()

    def delete_file(self):
        files_to_remove = [self.logfile, self.htmlpage]
        for filename in files_to_remove:
            try:
                os.remove(filename)
            except OSError:
                pass

    def dummy_logging(self, log):
        log.info("sample log first line")
        log.debug("sample log second line for debug")
        log.debug("sample log third line")

    def test_setup_logging(self):
        # for verbose
        log = setup_logging(logfile=self.logfile, verbose=True)
        self.dummy_logging(log)

        self.assertTrue(os.path.isfile(self.logfile), "log file creation")
        self.assertEqual(get_number_of_lines(self.logfile), 4,
                         "log files has lines")
        truncate_log_file(self.logfile)

        # for non verbose
        log = setup_logging(logfile=self.logfile)
        self.dummy_logging(log)

        self.assertTrue(os.path.isfile(self.logfile), "log file creation")
        self.assertEqual(get_number_of_lines(self.logfile), 2,
                         "There is only one line with info param")
        truncate_log_file(self.logfile)

    def test_parse_date(self):
        parsed_dt = parse_date("1/1/2017")
        self.assertEqual(parsed_dt.year, 2017, "gives the parsed date")

        parsed_dt = parse_date("Jan 1, 2017")
        self.assertEqual(parsed_dt.month, 1, "parsing a verbose date")

        parsed_dt = parse_date("1/1/2017", to_string=True)
        self.assertEqual(parsed_dt, "01_01_2017", "gives the parsed string")

    def test_setup_date_range(self):
        dt_range = setup_date_range(start_date="1/1/2017",
                                    end_date="1/2/2017")
        self.assertEqual(dt_range,
                         (datetime.date(2017, 1, 1),
                          datetime.date(2017, 1, 2)),
                         "start and end date range")

        self.assertSequenceEqual(list(map(lambda x: x.day, dt_range)),
                                 [1, 2],
                                 "check if the days are fine")

    def test_file_is_empty(self):
        # create an empty file and check if it is empty or not
        filename = create_csv("dummy_file")
        empty = file_is_empty(filename)
        self.assertTrue(empty, "file is empty")

        # the created file should be removed so that
        # the next test run is not currupted
        os.remove(filename)

        # check for file when the file is not empty
        def create_a_non_empty_file():
            with open("/tmp/non_empty_file", "w") as f:
                f.write("abcd")
        create_a_non_empty_file()
        non_empty_file = "/tmp/non_empty_file"
        empty = file_is_empty(non_empty_file)
        self.assertFalse(empty, "file is empty")
        os.remove("/tmp/non_empty_file")

    def test_create_csv(self):
        # file does not exist. So it should create the file.
        file_created = create_csv("dummy_file")

        self.assertTrue(os.path.isfile(file_created), "html page creation")

        with open(file_created, "w") as f:
            f.write("1")

        # file already exists so it should create another file and truncate it
        # return None
        file_created = create_csv("dummy_file")
        self.assertTrue(file_is_empty(file_created))

    def test_save_to_csv(self):
        headers = ["a", "b", "c"]
        filename = "csv/dummy_file.csv"
        record = [{"a": 1, "b": 2, "c": 3}]

        csvfile = save_to_csv(headers, filename, record)

        self.assertTrue(os.path.isfile(csvfile), "csv file is created")

        def test_file(csvfile):
            with open(csvfile) as f:
                lines = f.readlines()
                self.assertEqual(lines[0].rstrip(),
                                 "a,b,c", "first line has the headers")
                self.assertEqual(lines[1].rstrip(),
                                 "1,2,3", "second line is the data")

        test_file(csvfile)
        os.remove(csvfile)

    def test_save_to_csv_generator(self):
        headers = ["a", "b", "c"]
        filename = "csv/dummy_file81.csv"
        r1 = {"a": 1, "b": 2, "c": 3}
        r2 = {"a": 11, "b": 22, "c": 33}
        record = (r for r in [r1, r2])  # generator

        csvfile = save_to_csv(headers, filename, record)

        self.assertTrue(os.path.isfile(csvfile))

        def test_file(csvfile):
            with open(csvfile) as f:
                lines = f.readlines()
                self.assertEqual(lines[0].rstrip(),
                                 "a,b,c", "first line has the headers")
                self.assertEqual(lines[1].rstrip(),
                                 "1,2,3", "second line is the data")
                self.assertEqual(lines[2].rstrip(),
                                 "11,22,33", "there is the third line also")

        test_file(csvfile)

        os.remove(csvfile)

    def test_get_remaining_apn_list(self):
        filename = "/tmp/dummy.txt"
        data = ["123", "234", "345", "456"]
        with open(filename, "w") as dummy_file:
            for item in data:
                dummy_file.write("%s\n" % item)

        def run_tests():
            self.assertEqual(get_remaining_apn_list("123", filename),
                             ["234", "345", "456"])
            self.assertEqual(get_remaining_apn_list("234", filename),
                             ["345", "456"])
            self.assertEqual(get_remaining_apn_list("345", filename), ["456"])
            self.assertEqual(get_remaining_apn_list("456", filename), [])

        run_tests()
        os.remove(filename)

    def test_add_identifiers(self):
        filename = 'a'
        arg1, arg2, arg3 = ('b', 'c', 'd')
        self.assertEqual(add_identifiers(filename, arg1, arg2, arg3),
                         'a_b_c_d')

    def test_date_range(self):
        res = list(date_range('02272017', '03032017'))
        self.assertEqual(res,
                         ['02272017', '02282017',
                          '03012017', '03022017', '03032017'])

        # if only one date is given
        res = list(date_range('02272017', '02272017'))
        self.assertEqual(res, ['02272017'])

    def test_is_production_server_nonprod(self):
        self.assertFalse(is_production_server())

    @patch('scraper.utils.getpass.getuser')
    def test_is_production_server_mockprod(self, mock_getuser):
        mock_getuser.return_value = 'localinsights-bots'
        self.assertTrue(is_production_server())


if __name__ == "__main__":
    unittest.main()
