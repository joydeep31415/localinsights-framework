============================
How to contribute to Scraper
============================

Thanks for considering contributing to Scraper.

Support questions
=================

To Do: complete the possible support questions

Reporting issues
================

- 

Submitting patches
==================

- 

Running the testsuite
---------------------

You probably want to set up a `virtualenv
<https://virtualenv.readthedocs.io/en/latest/index.html>`_.

You do not need any dependencies to run the test suite just to check if all the tests pass. So running the following command in the root directory is fine.

    python -m unittests discover


Running test coverage
---------------------

Test coverage is supported using the pytest coverage module. To look into the test coverage first install the dependencies in your virtual environment.

    pip install -r requirements_dev.txt

After this you can run the pytest command to look into the coverage.

    pytest --cov-report term-missing --cov=scraper tests/

Shows the terminal report

    tests/test_proxy_rotator.py .
    tests/test_scraper.py .......
    tests/test_user_agent_rotator.py ..
    tests/test_utils.py ............

    ----------- coverage: platform linux, python 3.5.2-final-0 -----------
    Name                            Stmts   Miss  Cover   Missing
    -------------------------------------------------------------
    scraper/__init__.py                66      5    92%   41, 45, 154-156
    scraper/proxy_rotator.py           14      0   100%
    scraper/user_agent_rotator.py      14      0   100%
    scraper/utils.py                  108     15    86%   29-42, 56, 100-101, 106, 109, 205-206, 245
    -------------------------------------------------------------
    TOTAL                             202     20    90%



cloning
-------


